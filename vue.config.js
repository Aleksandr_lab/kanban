const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  transpileDependencies: [
    'vuetify',
  ],
  pages: {
    index: {
      entry: 'src/main.js',
      template: 'resurses/index.html',
      title: 'Kanban',
      filename: 'index.html',
      chunks: ['chunk-vendors', 'chunk-common', 'index'],
    },
  },
  configureWebpack: {
    performance: {
      maxAssetSize: 500000,
    },
    plugins: [
      new CopyWebpackPlugin([
        { from: 'resurses/', to: '' },
      ]),
    ],
  },
  outputDir: 'public',
  publicPath: '/kanban/',

};
