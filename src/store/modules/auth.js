import firebase from 'firebase/app';

export default {
  state: {
    user: null,
    error: null,
  },
  mutations: {
    setAuthError(state, payload) {
      state.error = payload;
    },
  },
  actions: {
    async forgotPassword({ commit }, email) {
      commit('setAuthError', null);
      try {
        await firebase.auth().sendPasswordResetEmail(email);
      } catch ({ message }) {
        commit('setAuthError', message);
        throw message;
      }
    },
    async login({ commit }, { email, pass }) {
      try {
        await firebase.auth().signInWithEmailAndPassword(email, pass);
      } catch ({ message }) {
        commit('setAuthError', message);
        throw message;
      }
    },
    async logout() {
      await firebase.auth().signOut();
    },
    getUid() {
      const user = firebase.auth().currentUser;
      return user ? user.uid : null;
    },
    async register({ dispatch, commit }, { email, pass, name }) {
      try {
        await firebase.auth()
          .createUserWithEmailAndPassword(email, pass);
        const uid = await dispatch('getUid');
        await firebase.database()
          .ref(`/users/${uid}/info`)
          .set({
            name,
          });
      } catch ({ message }) {
        commit('setAuthError', message);
        throw message;
      }
    },
  },
};
