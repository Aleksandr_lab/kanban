import firebase from 'firebase/app';

export default {
  async getBoard({ dispatch, commit }) {
    try {
      const uid = await dispatch('getUid');
      const data = (await firebase.database().ref(`/users/${uid}/task`).once('value')).val();
      const board = data ? JSON.parse(data) : [];
      commit('updateBoard', board);
    } catch ({ message }) {
      commit('setErrorBoard', message);
    }
  },
  async updateBoard({ dispatch, commit, state }) {
    try {
      const uid = await dispatch('getUid');
      await firebase.database().ref(`/users/${uid}/task`).set(JSON.stringify(state.boardItems));
    } catch ({ message }) {
      commit('setErrorBoard', message);
    }
  },
};
