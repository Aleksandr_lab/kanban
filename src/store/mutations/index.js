import CreateColumn from '../../utils/CreateColumn';
import CreateCard from '../../utils/CreateCard';

export default {
  setErrorBoard(state, payload) {
    state.error = payload;
  },
  updateBoard(state, payload) {
    state.boardItems = payload;
  },
  removeColumn(state, columnId) {
    state.boardItems = state.boardItems.filter(({ id }) => id !== columnId);
  },
  addColumn(state, title) {
    state.boardItems.push(new CreateColumn(title));
  },
  changeColumnTitle(state, { title, columnId }) {
    state.boardItems = state.boardItems.map((el) => {
      const newEl = el;
      if (newEl.id === columnId) {
        newEl.title = title;
      }
      return newEl;
    });
  },
  removeCard(state, item) {
    const idx = state.boardItems.findIndex(({ id }) => id === item.parentId);
    state.boardItems[idx].card = state.boardItems[idx].card.filter(({ id }) => id !== item.id);
  },
  addCard(state, { parentId, title }) {
    state.boardItems = state.boardItems.map((el) => {
      if (el.id === parentId) {
        el.card.push(new CreateCard(title, parentId));
      }
      return el;
    });
  },
  changeCard(state, item) {
    const idx = state.boardItems.findIndex(({ id }) => id === item.parentId);
    state.boardItems[idx].card = state.boardItems[idx].card.map((el) => {
      const newItem = el;
      if (newItem.id === item.id) {
        newItem.title = item.title;
      }
      return newItem;
    });
  },
  setColumnCard(state, { items, parentId }) {
    const idx = state.boardItems.findIndex(({ id }) => id === parentId);
    state.boardItems[idx].card = items;
  },
};
