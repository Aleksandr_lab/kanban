import { v4 as uuidv4 } from 'uuid';

export default class CreateCard {
  constructor(title, parentId) {
    this.id = uuidv4();
    this.parentId = parentId;
    this.title = title;
  }
}
