import { v4 as uuidv4 } from 'uuid';

export default class CreateColumn {
  constructor(title, order, card = [], user = null) {
    this.id = uuidv4();
    this.title = title;
    this.card = card;
    this.user = user;
  }
}
