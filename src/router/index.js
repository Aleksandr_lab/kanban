import Vue from 'vue';
import VueRouter from 'vue-router';
import firebase from 'firebase/app';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: { auth: true },
    component: () => import(/* Home */ '../views/Home.vue'),
  },
  {
    path: '/login/',
    name: 'Login',
    component: () => import(/* Login */ '../views/Login.vue'),
    children: [
      {
        name: 'SignIn',
        path: 'sign-in',
        component: () => import(/* SignIn */ '../components/login/organism/SignIn.vue'),
      },
      {
        name: 'SignUp',
        path: 'sign-up',
        component: () => import(/* SignUp */ '../components/login/organism/SignUp.vue'),
      },
      {
        name: 'ForgotPassword',
        path: 'forgot-password',
        component: () => import(/* ForgotPassword */ '../components/login/organism/ForgotPassword.vue'),
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const { currentUser } = firebase.auth();
  const isAuth = to.meta.auth;

  if (isAuth && !currentUser) {
    next('/login/sign-in?error=error-login');
  } else {
    next();
  }
});

export default router;
