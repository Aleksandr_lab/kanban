import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

import './assets/css/app.scss';
import Vue from 'vue';
import './directive';
import PerfectScrollbar from 'vue2-perfect-scrollbar';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import vuetify from './plugins/vuetify';

Vue.use(PerfectScrollbar);

firebase.initializeApp({
  apiKey: 'AIzaSyDJLfMzHu-HTbn9p5jEn2DSfdZKuDdE818',
  authDomain: 'vue-task-dashboard.firebaseapp.com',
  databaseURL: 'https://vue-task-dashboard.firebaseio.com',
  projectId: 'vue-task-dashboard',
  storageBucket: 'vue-task-dashboard.appspot.com',
  messagingSenderId: '722439760495',
  appId: '1:722439760495:web:d087b18ffaecf2399f7b9a',
  measurementId: 'G-1WV7RBZXVP',
});

Vue.config.productionTip = false;

let app;

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      vuetify,
      render: (h) => h(App),
    }).$mount('#app');
  }
});
